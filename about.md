---
layout: page
title: About
permalink: /about/
---

The content that you see is in a md file. It's really nice to have your work in plain text to work upon. Jekyll, the Static Site Generator (SSG) will render it automatically.
The website can be mainted as markdown files, and all we would worry about is text only.